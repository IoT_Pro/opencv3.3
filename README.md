# OpenCV 3.3.1 install Guide on RPi3

## Install package

    sudo apt-get install build-essential git cmake pkg-config
    sudo apt-get install libjpeg-dev libtiff5-dev libjasper-dev libpng12-dev
    sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev
    sudo apt-get install libxvidcore-dev libx264-dev
    sudo apt-get install pkg-config
    sudo apt-get install libgtk2.0-dev
    sudo apt-get install libatlas-base-dev gfortran
    
## Download opencv 3.3.1 and opencv-contrib 3.3.1

	cd ~
	wget -O opencv.zip https://github.com/Itseez/opencv/archive/3.3.1.zip
	unzip opencv.zip
	
	wget -O opencv_contrib.zip https://github.com/Itseez/opencv_contrib/archive/3.3.1.zip
	unzip opencv_contrib.zip

    Note: remove xfeatures2d directory from  opencv-contrib 3.3.1/modules.
    xfeatures2d/src/boostdesc.cpp file makes error during make.
    
## Install numpy >= 1.11 and check numpy version

    sudo apt-get remove python3-numpy
    sudo pip3 install --upgrade numpy
	
	import numpy
	numpy.__version__ (or numpy.version.version)
    
## make and install opencv

    cd ~/opencv
    mkdir build
    cd build
    cmake -D CMAKE_BUILD_TYPE=RELEASE \
        -D CMAKE_INSTALL_PREFIX=/usr/local \
        -D ENABLE_PRECOMPILED_HEADERS=OFF \
        -D INSTALL_C_EXAMPLES=OFF \
        -D INSTALL_PYTHON_EXAMPLES=ON \
        -D OPENCV_EXTRA_MODULES_PATH=~/opencv_contrib/modules \
        -D BUILD_EXAMPLES=ON ..
    make 
    sudo make install
    sudo ldconfig